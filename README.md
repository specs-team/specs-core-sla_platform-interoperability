# Interoperability Layer Description #
The Interoperability layer offers functionalities for enabling transparent communication among different modules. In practice, it acts as a gateway by intercepting all API calls and by redirecting them to the right component. This is accomplished by defining a suitable virtual interface that associates a set of API calls to a specific end-point (i.e., to a specific URL).

### Use Cases ###

![Use Case.png](https://bitbucket.org/repo/qqxnyX/images/986780721-Use%20Case.png)

## Installation ##

**Install using precompiled binaries**

The precompiled binaries are available under the SPECS Artifact Repository (http://ftp.specs-project.eu/public/artifacts/)

Prerequisites:

* Oracle Java JDK 7;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);

Installation steps:

* download the web application archive (war) file from the artifact repository :
http://ftp.specs-project.eu/public/artifacts/sla-platform/interoperability/interoperability-api-STABLE.war
* the war file has to be deployed in the java servlet/web container

If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

**Compile and install from source**

The following are the prerequisites to compile and install the Interoperability Layer:

Prerequisites:

* a Git client;
* Apache Maven 3.3.x;
* Oracle Java JDK 7;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);

Installation steps:

* clone the Bitbucket repository:
```
#!bash
git clone git@bitbucket.org:specs-team/specs-core-sla_platform-interoperability.git
```
* under specs-core-sla_platform-interoperability run:

```
#!bash
mvn package
```

The installation steps generate a web application archive (war) file, under the “/target” subfolder. In order to use the component, the war file has to be deployed in the java servlet/web container. If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

## Usage ##
The Interoperatibility component exposes a REST API interface. All the exposed REST resources are mapped under the path “/cloud-sla/*”. The mapping rules are defined in the web.xml file under WEB-INF folder (CATALINA_HOME/webapps/service-manager-api/WEB-INF/):


```
#!xml
<servlet-mapping>
		<servlet-name>Jersey REST Service</servlet-name>
		<url-pattern>/*</url-pattern>
</servlet-mapping>
```

Moreover, all the REST resources are represented by specific java classes under the package “eu.specsproject.slaplatform.slainteroperability.restfrontend”, defined in the web.xml file under WEB-INF folder:

```
#!xml
<init-param>
	<param-name>jersey.config.server.provider.packages</param-name>
	<param-value>eu.specsproject.slaplatform.slainteroperability.restfrontend, org.codehaus.jackson.jaxrs</param-value>
</init-param>
```

At the startup of the component, all the REST resources are configured based on the configuration parameters defined in the web.xml file.

### Who do I talk to? ###

* Please contact massimiliano.rak@unina2.it
* www.specs-project.eu

SPECS Project 2013 - CeRICT