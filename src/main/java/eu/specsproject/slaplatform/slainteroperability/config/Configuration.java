package eu.specsproject.slaplatform.slainteroperability.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;

import eu.specsproject.slaplatform.slainteroperability.manager.VirtualInterface;
import eu.specsproject.slaplatform.slainteroperability.utility.ProxyController;

public class Configuration {
	@Context
	ServletContext servletContext;
	
	private static Configuration instance = null;
	private boolean eventEnabled = false;
	private int maxEvent = 10000;
	private boolean roundrobin = false; // true Round Robin / 0 Packet loss

	public static Configuration getInstance() {
		if (instance == null) {
			
			instance = new Configuration();
		}
		return instance;
	}
	

	public Configuration() {

	}
	
	public void setEventConf(boolean enabled, int maxEvent, boolean roundrobin) {
		this.eventEnabled = enabled;
		this.maxEvent = maxEvent;
		this.roundrobin = roundrobin;
	}
	
	public boolean getEventEnabled() {
		return this.eventEnabled;
	}
	
	public boolean getRoundrobin() {
		return this.roundrobin;
	}
	
	public int getEventsSize() {
		return this.maxEvent;
	}

}
