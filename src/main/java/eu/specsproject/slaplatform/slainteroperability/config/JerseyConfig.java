package eu.specsproject.slaplatform.slainteroperability.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;

@ApplicationPath("/")
public class JerseyConfig extends Application {
	@Context
	ServletContext servletContext;

	public static Properties properties = new Properties();

	private Properties readProperties() {

		InputStream is = servletContext
				.getResourceAsStream("/WEB-INF/settings.js");

		if (is != null) {
			try {
				properties.load(is);
				Configuration.getInstance().setEventConf(
						Boolean.valueOf(properties.getProperty("enabled")),
						Integer.valueOf(properties.getProperty("size")),
						Boolean.valueOf(properties.getProperty("roundrobin")));
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return properties;
	}

	@Override
	public Set<Class<?>> getClasses() {

		readProperties();

		// Set up your Jersey resources
		Set<Class<?>> rootResources = new HashSet<Class<?>>();
		rootResources.add(Configuration.class);
		return rootResources;
	}
}