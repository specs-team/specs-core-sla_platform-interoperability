package eu.specsproject.slaplatform.slainteroperability.restfrontend;

import java.lang.annotation.Annotation;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.client.ClientConfig;

import eu.specsproject.slaplatform.slainteroperability.config.Configuration;
import eu.specsproject.slaplatform.slainteroperability.manager.IN_Manager;
import eu.specsproject.slaplatform.slainteroperability.restfrontend.IN_Events;
import eu.specsproject.slaplatform.slainteroperability.restfrontend.IN_Resource;
import eu.specsproject.slaplatform.slainteroperability.utility.Event;
import eu.specsproject.slaplatform.slainteroperability.utility.EventManager;
import eu.specsproject.slaplatform.slainteroperability.utility.ProxyController;

@Path("/")
public class INs_Resource {
	@Context
    UriInfo uriInfo;

	@GET
	@Path("{subResources:.*}")
	public String get() 
	{	    
	    Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getGenericHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).get();
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
	
	@POST
	@Path("{subResources:.*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String post(@Context UriInfo uriInfo, String json) 
	{
	    Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getGenericHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).post(Entity.json(json));
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
	@PUT
	@Path("{subResources:.*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String put(@Context UriInfo uriInfo, String json) 
	{
	    Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getGenericHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).put(Entity.json(json));
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
	@DELETE
	@Path("{subResources:.*}")
	@Produces(MediaType.TEXT_PLAIN)
	public String delete(@Context UriInfo uriInfo) 
	{
	    Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getGenericHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).delete();
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getSlas(@Context UriInfo uriInfo) {
		return "HOME";	
	}

	@GET
	@Path("/{param1}/slas")
	public String getSlas(@Context UriInfo uriInfo, @PathParam("id") String id) {
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		System.out.println("PATH A CUI FACCIO LA GET: "+ProxyController.getInstance().getHttpTargetUrl(uriInfo));
		Response response = client.target(ProxyController.getInstance().getHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).get();
		//EventManager.getInstance().addEvent(new Event());
		System.out.println("Respose to request: "+response.getEntity().toString());
		return response.getEntity().toString();
	}
	
	@POST
	@Path("/{param1}/slas")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String createSlas(@Context UriInfo uriInfo, String json) {
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).post(Entity.json(json));
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
	@Path("/interoperability")
	public IN_Manager getManager(@Context UriInfo uriInfo, @PathParam("id") String id) {
		System.out.println("manager URI " + ((uriInfo == null)?"NUL":"NONULL") + uriInfo.getAbsolutePath().toASCIIString());
		return new IN_Manager();
	}
	
	@Path("/event")
	public IN_Manager getEvents(@Context UriInfo uriInfo, @PathParam("id") String id) {
		System.out.println("manager URI " + ((uriInfo == null)?"NUL":"NONULL") + uriInfo.getAbsolutePath().toASCIIString());
		return new IN_Events();
	}
	
	@Path("/{param1}/slas/{id}")
	public IN_Resource getSlas(@PathParam("id") String id) {
		return new IN_Resource(id);
	}
}
