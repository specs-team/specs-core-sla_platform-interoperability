package eu.specsproject.slaplatform.slainteroperability.restfrontend;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.client.ClientConfig;

import eu.specsproject.slaplatform.slainteroperability.utility.ProxyController;

public class IN_AssociationsResource {
	@Context
    UriInfo uriInfo;
	
	private int id;
	public IN_AssociationsResource(int id) {
		this.id = id;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAssociations(@Context UriInfo uriInfo) {
		//SSLContext ssl = ... your configured SSL context;
		//sClient client = ClientBuilder.newBuilder().sslContext(ssl).build();
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).get();
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
}
