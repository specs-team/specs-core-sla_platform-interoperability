package eu.specsproject.slaplatform.slainteroperability.restfrontend;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.client.ClientConfig;

import eu.specsproject.slaplatform.slainteroperability.utility.ProxyController;

public class IN_AlertsResource {
	@Context
    UriInfo uriInfo;
	
	private int id;
	public IN_AlertsResource(int id) {
		this.id = id;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAlert(@Context UriInfo uriInfo) {
		//EventManager.getInstance().addEvent(new Event());
		//return ProxyController.getInstance().getHttpTargetUrl(uriInfo);
		//SSLContext ssl = ... your configured SSL context;
		//sClient client = ClientBuilder.newBuilder().sslContext(ssl).build();
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).get();
		return response.getEntity().toString();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String createSlas(@Context UriInfo uriInfo, String json) {
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).post(Entity.json(json));
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
}
