package eu.specsproject.slaplatform.slainteroperability.restfrontend;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;

import eu.specsproject.slaplatform.slainteroperability.manager.IN_Manager;
import eu.specsproject.slaplatform.slainteroperability.utility.Event;
import eu.specsproject.slaplatform.slainteroperability.utility.EventManager;

@Path("IN_Events")
public class IN_Events extends IN_Manager {
	UriInfo uriInfo;

	public IN_Events() {

	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInterface(@PathParam("id") String id) {
		return (new Gson().toJson(EventManager.getInstance().getEvent(id)));
	}
	
	@GET
	@Path("/events")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInterfaces(@Context UriInfo uriInfo) {
		Collection<String> ids = new ArrayList<String>();
		
		Collection<Event> interfaces = EventManager.getInstance()
				.getAllEvents();
		Iterator<Event> litr = interfaces.iterator();
		while (litr.hasNext()) {
			Event element = (Event) litr.next();
			//ids.add(element.getId());
		}
		return (new Gson().toJson(ids));

	}
}
