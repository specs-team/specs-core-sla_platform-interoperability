package eu.specsproject.slaplatform.slainteroperability.restfrontend;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.ws.spi.http.HttpContext;

import org.glassfish.jersey.client.ClientConfig;

import eu.specsproject.slaplatform.slainteroperability.utility.ProxyController;


public class IN_Resource {
	@Context
    UriInfo uriInfo;
	
	private int id;
	
	public IN_Resource (String id){
		this.id = Integer.valueOf(id);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getResource(@Context UriInfo uriInfo){
		
		//SSLContext ssl = ... your configured SSL context;
		//sClient client = ClientBuilder.newBuilder().sslContext(ssl).build();
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).get();
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String createResources(@Context UriInfo uriInfo, String json) {
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		Response response = client.target(ProxyController.getInstance().getHttpTargetUrl(uriInfo)).request(MediaType.APPLICATION_JSON).put(Entity.json(json));
		//EventManager.getInstance().addEvent(new Event());
		return response.getEntity().toString();
	}
	
	/*
	@Path("/annotations")
	public IN_AnnotationsResource getAnnotationsResource(){
		System.out.println("URI " + ((uriInfo == null)?"NUL":"NONULL") + uriInfo.getAbsolutePath().toASCIIString());
		return new IN_AnnotationsResource(id);
	}
	*/
	
	@Path("/alerts")
	public IN_AlertsResource getAlertsResource(){
		return new IN_AlertsResource(id);
	}
	
	@Path("/alerts/{param}")
	public IN_AlertsResource getAlertsIDResource(){
		return new IN_AlertsResource(id);
	}
	
	@Path("/violations")
	public IN_ViolationsResource getViolatiosResource(){
		return new IN_ViolationsResource(id);
	}
	
	@Path("/violations/{param}")
	public IN_ViolationsResource getViolatiosIdResource(){
		return new IN_ViolationsResource(id);
	}
	
	@Path("/templates")
	public IN_TemplateResource getTemplatesResource(){
		return new IN_TemplateResource(id);
	}
	
	@Path("/templates/{param}")
	public IN_TemplateResource getTemplatesIdResource(){
		return new IN_TemplateResource(id);
	}
	
	@Path("/associations")
	public IN_AssociationsResource getAssociationsResource(){
		return new IN_AssociationsResource(id);
	}
	
	@Path("/associations/{param}")
	public IN_AssociationsResource getAssociationsIdResource(){
		return new IN_AssociationsResource(id);
	}
	
	@Path("/alert")
	public IN_AlertResource getAlertResource(){
		return new IN_AlertResource(id);
	}
	
	@Path("/violation")
	public IN_ViolationResource getViolationResource(){
		return new IN_ViolationResource(id);
	}
	
	@Path("/template")
	public IN_TemplateResource getTemplateResource(){
		return new IN_TemplateResource(id);
	}
	
	@Path("/association")
	public IN_Association getAssociationResource(){
		return new IN_Association(id);
	}
	
	@Path("/state")
	public IN_StateResource getStateResource(){
		return new IN_StateResource(id);
	}
	
}
