package eu.specsproject.slaplatform.slainteroperability.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.UriInfo;

import eu.specsproject.slaplatform.slainteroperability.config.Configuration;
import eu.specsproject.slaplatform.slainteroperability.manager.VirtualInterface;

public class ProxyController {

	private static ProxyController instance = null;
	private Map<String, VirtualInterface> contentProvider = new HashMap<String, VirtualInterface>();

	public static ProxyController getInstance() {
		if (instance == null) {
			instance = new ProxyController();
		}
		return instance;
	}

	public String getHttpTargetUrl(UriInfo uriInfo) {
		String targetUrl = buildTargetUrl(uriInfo);
		if (targetUrl.isEmpty())
			return null;
		else
			return ("http://" + targetUrl);
	}

	public String getHttpsTargetUrl(UriInfo uriInfo) {
		String targetUrl = buildTargetUrl(uriInfo);
		if (targetUrl.isEmpty())
			return null;
		else
			return ("https://" + targetUrl);
	}

	private String buildTargetUrl(UriInfo uriInfo) {
		System.out.println("uriInfo get Absolute path: "
				+ uriInfo.getAbsolutePath());
		String base = uriInfo.getAbsolutePath().toASCIIString();
		base = base.replace(uriInfo.getBaseUri().toASCIIString(), "");

		int pos = base.indexOf("/");
		base = base.substring(0, pos);

		String parameters = uriInfo.getAbsolutePath().toASCIIString();
		parameters = parameters.replace(
				(uriInfo.getBaseUri().toASCIIString() + base), "");

		String targetUrl = null;
		System.out.println("uriInfo get Absolute base: " + base);
		VirtualInterface virtualInterface = getInterface(base);
		if (virtualInterface != null) {
			targetUrl = virtualInterface.getDestination_address();
			if (virtualInterface.getDestination_port().length() > 0) {
				targetUrl = ":" + virtualInterface.getDestination_port();
			}
			targetUrl += "/" + base + parameters;
		}

		return targetUrl;
	}

	public void addInterface(VirtualInterface virtualInterface) {
		System.out.println("CALL ADD INTERFACE WITH ID: "
				+ virtualInterface.getId());
		contentProvider.put(virtualInterface.getId(), virtualInterface);
	}

	public boolean putInterface(VirtualInterface virtualInterface) {
		if (contentProvider.containsKey(virtualInterface.getId())) {
			contentProvider.put(virtualInterface.getId(), virtualInterface);
			return true;
		}
		return false;
	}

	public VirtualInterface deleteInterface(String key) {
		return contentProvider.remove(key);
	}

	public VirtualInterface getInterface(String key) {
		System.out.println("CALL GET INTERFACE BY ID: " + key);
		System.out.println("FOUND?: " + contentProvider.get(key));

		return contentProvider.get(key);
	}

	public Collection<VirtualInterface> getAllInterfaces() {
		return contentProvider.values();
	}

	public String getGenericHttpTargetUrl(UriInfo uriInfo) {
		String path = uriInfo.getAbsolutePath().toASCIIString();
		path = path.replace(uriInfo.getBaseUri().toASCIIString(), "");
		List<String> pathList = new ArrayList<String>(Arrays.asList(path
				.split("/")));

		int max = 0;
		int current = 0;
		String id = "";
		
		for (Map.Entry<String, VirtualInterface> entry : contentProvider.entrySet())
		{
		    VirtualInterface currentInterface =  entry.getValue();
		    String baseInterface = currentInterface.getBase_url();
			List<String> list = new ArrayList<String>(Arrays.asList(baseInterface.split("/")));
			current = retainCommonWithBase(list, pathList);
			if (current > max) {
				max = current;
				id = currentInterface.getId();
			}
		}
		
		String targetUrl = "";

		VirtualInterface virtualInterface = getInterface(id);
		if (virtualInterface != null) {
			targetUrl = virtualInterface.getDestination_address();
			if (virtualInterface.getDestination_port().length() > 0) {
				targetUrl += ":" + virtualInterface.getDestination_port();
			}
			targetUrl += "/" + virtualInterface.getDestination_uri() + "/" + path;
		}
		return targetUrl;
	}

	static int retainCommonWithBase(List<String> strings, List<String> base) {
		int min = Integer.min(strings.size(), base.size());
		for (int i = 0; i < min; i++) {
			if (strings.get(i).equals(base.get(i)) == false)
				return i;
		}
		return min;
	}
}
