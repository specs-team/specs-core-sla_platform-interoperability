package eu.specsproject.slaplatform.slainteroperability.utility;

public class ItemList {
	private String item;
	private String id;
	
	public ItemList(String id, String item) {
		this.id = id;
		this.item = item;
	}
	
	public ItemList() {
		
	}
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
