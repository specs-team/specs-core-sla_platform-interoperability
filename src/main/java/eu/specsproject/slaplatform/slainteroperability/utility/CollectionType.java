package eu.specsproject.slaplatform.slainteroperability.utility;

import java.util.List;

public class CollectionType {
	private String resource;
	private String total;
	private String members;
	private List <ItemList> itemList;
	
	public CollectionType(String resource, String total, String members, List<ItemList> itemList) {
		this.resource = resource;
		this.total = total;
		this.members = members;
		this.itemList = itemList;
	}
	
	public CollectionType() {
		
	}
	
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getMembers() {
		return members;
	}
	public void setMembers(String members) {
		this.members = members;
	}
	public List<ItemList> getItemList() {
		return itemList;
	}
	public void setItemList(List<ItemList> itemList) {
		this.itemList = itemList;
	}
	
	
}
