package eu.specsproject.slaplatform.slainteroperability.utility;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.JsonElement;

import eu.specsproject.slaplatform.slainteroperability.config.Configuration;

public class EventManager {
	private static EventManager instance = null;
	private int currentElement = -1; 
	private ArrayList<Event> events= new ArrayList<Event>();
	private boolean enabled;
	private int maxEvents;
	private boolean roundrobin;
	
	public static EventManager getInstance() {
		if (instance == null) {	
			instance = new EventManager();
		}
		return instance;
	}
	
	public EventManager() {
		enabled = Configuration.getInstance().getEventEnabled();
		maxEvents = Configuration.getInstance().getEventsSize();
		roundrobin = Configuration.getInstance().getRoundrobin();
	}
	
	public void addEvent(Event event) {
		if (!enabled)
			return;
		
		if((!roundrobin && events.size() < maxEvents) ||
				(roundrobin && events.size() < maxEvents)) {
			events.add(event);
			currentElement = events.size() - 1;
		} else {
			currentElement = (currentElement++) % maxEvents;
			events.set(currentElement, event);
		}
	}

	public Event getEvent(String id) {
		int pos = Integer.valueOf(id);
		if (pos >= 0 && pos <= events.size())
			return events.get(pos);
		return null;
		
	}

	public Collection<Event> getAllEvents() {
		return events;
	}
	
}
