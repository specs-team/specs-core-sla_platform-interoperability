package eu.specsproject.slaplatform.slainteroperability.manager;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;

import eu.specsproject.slaplatform.slainteroperability.manager.VirtualInterface;
import eu.specsproject.slaplatform.slainteroperability.utility.CollectionType;
import eu.specsproject.slaplatform.slainteroperability.utility.ItemList;
import eu.specsproject.slaplatform.slainteroperability.utility.ProxyController;

public class IN_Manager {
	@Context
	UriInfo uriInfo;

	public IN_Manager() {

	}

	@GET
	@Path("/interfaces/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInterface(@PathParam("id") String id) {
		return (new Gson().toJson(ProxyController.getInstance().getInterface(id)));
	}

	
	@DELETE
	@Path("/interfaces/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteInterface(@PathParam("id") String id, @Context UriInfo uriInfo) {
		if (ProxyController.getInstance().deleteInterface(id) != null)
			return Response.status(Status.NO_CONTENT).build();
		else
			return Response.status(Status.NOT_FOUND).build();
	}
	
	@POST
	@Path("/interfaces")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response createInterface(VirtualInterface virtualInterface,
			@Context UriInfo uriInfo) {

		ProxyController.getInstance().addInterface(virtualInterface);

		UriBuilder ub = uriInfo.getAbsolutePathBuilder();
		URI smURI = ub.path(virtualInterface.getId()).build();

		return Response.created(smURI).entity(virtualInterface.getId()).build();
	}

	@PUT
	@Path("/interfaces")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response updateInterface(VirtualInterface virtualInterface,
			@Context UriInfo uriInfo) {

	    if (ProxyController.getInstance().putInterface(virtualInterface) == false)
			return Response.status(Status.NOT_FOUND).build();

		UriBuilder ub = uriInfo.getAbsolutePathBuilder();
		URI smURI = ub.path(virtualInterface.getId()).build();

		return Response.created(smURI).entity(virtualInterface.getId()).build();
	}

	@GET
	@Path("/interfaces")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInterfaces(@Context UriInfo uriInfo) {
		
		Collection<VirtualInterface> interfaces = ProxyController.getInstance()
				.getAllInterfaces();
		Iterator<VirtualInterface> litr = interfaces.iterator();
		List<ItemList> itemList = new ArrayList<ItemList>();
		while (litr.hasNext()) {
			VirtualInterface element = (VirtualInterface) litr.next();
			itemList.add(new ItemList(element.getId(), "http://localhost:8080/interoperability-api/interoperability/interfaces/" + element.getId()));
		}
		CollectionType collectionType = new CollectionType("virtualinterface", Integer.toString(itemList.size()), Integer.toString(itemList.size()), itemList);
		return (new Gson().toJson(collectionType));

	}
}
