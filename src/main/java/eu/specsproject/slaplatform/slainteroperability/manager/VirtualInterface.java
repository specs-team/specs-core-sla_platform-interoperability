package eu.specsproject.slaplatform.slainteroperability.manager;



public class VirtualInterface  {

	private String id;
	private String name;
	
	private String base_url;
	
	private String destination_address;
	
	private String destination_port;
	
	private String destination_uri;
	
	public VirtualInterface() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String toString() {
		return new StringBuffer(" Name: ").append(this.name)
		.append(" id: ").append(this.id)
		.append(" baseUrl: ").append(this.base_url).append(" Destination Address : ")
		.append(this.destination_address).append(" Destination POrt :").append(this.destination_port).append(" Destination uri: ").append(this.destination_uri).toString();
	}

    public String getBase_url() {
        return base_url;
    }

    public void setBase_url(String base_url) {
        this.base_url = base_url;
    }

    public String getDestination_address() {
        return destination_address;
    }

    public void setDestination_address(String destination_address) {
        this.destination_address = destination_address;
    }

    public String getDestination_port() {
        return destination_port;
    }

    public void setDestination_port(String destination_port) {
        this.destination_port = destination_port;
    }

    public String getDestination_uri() {
        return destination_uri;
    }

    public void setDestination_uri(String destination_uri) {
        this.destination_uri = destination_uri;
    }
}
