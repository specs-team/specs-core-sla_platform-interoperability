package eu.specsproject.slaplatform.slainteroperability.test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

import eu.specsproject.slaplatform.slainteroperability.manager.VirtualInterface;
import eu.specsproject.slaplatform.slainteroperability.restfrontend.INs_Resource;
import eu.specsproject.slaplatform.slainteroperability.utility.Event;
import eu.specsproject.slaplatform.slainteroperability.utility.EventManager;
import eu.specsproject.slaplatform.slainteroperability.utility.ProxyController;
import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class INs_ResourceTest extends JerseyTest{
    private static VirtualInterface vinterface ;
    private static EventManager emanager ;
    private static ProxyController controller;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");

        vinterface = new VirtualInterface();
        Assert.assertNotNull(vinterface);
        vinterface.setBase_url("url");
        vinterface.setDestination_address("destinatioN");
        vinterface.setId("1");
        vinterface.setName("interfaccia");
        vinterface.setDestination_port("porta");
        vinterface.setDestination_uri("tentative");
        Assert.assertNotNull(vinterface.toString());
        emanager = EventManager.getInstance();
        Assert.assertNotNull(emanager);
        controller = ProxyController.getInstance();
        Assert.assertNotNull(controller);



    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //do nothing
    }

    @Before
    public void setUpChild() { }

    @After
    public void tearDownChild() { }

    @Override
    public Application configure() {
        return new ResourceConfig(INs_Resource.class);
    }
    // TEST DELLA CLASSE EVENT
    @Test
    public final void testEvent(){
        Event e = new Event ();
        Assert.assertNotNull(e);
    }
    // TEST DELLA CLASSE VIRTUAL INTERFACE
    @Test
    public final void testVirtualInterface(){
        Assert.assertNotNull(vinterface);

    }
    @Test
    public final void testName(){
        Assert.assertNotNull(vinterface.getName());
        vinterface.setName("nome");
        Assert.assertEquals("nome",vinterface.getName());
    }
    @Test
    public final void testId(){
        Assert.assertNotNull(vinterface.getId());
        vinterface.setId("myId");
        Assert.assertEquals("myId", vinterface.getId());
    }
    @Test
    public final void testBaseUrl(){
        Assert.assertNotNull(vinterface.getBase_url());
        vinterface.setBase_url("baseURL");
        Assert.assertEquals("baseURL", vinterface.getBase_url());
    }
    @Test
    public final void testDestinationAddress(){
        Assert.assertNotNull(vinterface.getDestination_address());
        vinterface.setDestination_address("myDestinationAddress");
        Assert.assertEquals("myDestinationAddress", vinterface.getDestination_address());
    }
    @Test
    public final void testDestinationPort(){
        Assert.assertNotNull(vinterface.getDestination_port());
        vinterface.setDestination_port("myDestinationPort");
        Assert.assertEquals("myDestinationPort", vinterface.getDestination_port());
    }
    @Test
    public final void testDestinationURI(){
        Assert.assertNotNull(vinterface.getDestination_uri());
        vinterface.setDestination_uri("myDestinationURI");
        Assert.assertEquals("myDestinationURI", vinterface.getDestination_uri());
    }
    @Test
    public final void testToString(){
        Assert.assertNotNull(vinterface.toString());
    }

    // Test della classe EVENT MANAGER
    @Test
    public final void testIstance(){
        Assert.assertNotNull(emanager);
    }
    @Test
    public final void testAdd(){
        emanager.addEvent(new Event());

    }
    @Test
    public final void testGetEvent() {

        Assert.assertNull(emanager.getEvent("100")); 
    }
    @Test
    public final void testGetAllEvents(){
        Assert.assertNotNull(emanager.getAllEvents());
    }

    // TEST DELLA CLASSE PROXYCONTROLLER
    @Test
    public final void testAddInterface(){
        controller.addInterface(vinterface);  
        Assert.assertTrue(true);
    }
    @Test
    public final void testPutInterface(){
        controller.addInterface(vinterface);
        Assert.assertTrue(controller.putInterface(vinterface));
        VirtualInterface v2 = new VirtualInterface();
        v2.setId("prova2");
        Assert.assertFalse(controller.putInterface(v2));    
    }
    @Test
    public final void testGetInterface(){
        VirtualInterface v2 = new VirtualInterface();
        v2.setId("prova2");
        controller.addInterface(v2);
        Assert.assertNotNull(controller.getInterface("prova2"));
        controller.deleteInterface("prova2");
    }
    @Test
    public final void testDeleteInterface(){
        VirtualInterface v2 = new VirtualInterface();
        v2.setId("provadelete");
        controller.addInterface(v2);
        Assert.assertNotNull(controller.deleteInterface("provadelete"));
    }
    @Test
    public final void testGetAllInterfaces(){
        Assert.assertNotNull(controller.getAllInterfaces());
    }






    @Test
    public final void getHomeTest() {
        String response = target("").request().get(String.class);
        System.out.println("Response test: "+response);
        Assert.assertEquals("HOME", response);
    }

    @Test
    public final void firstGetInterfacesTest() {
        String response = target("/interoperability/interfaces").request().get(String.class);
        System.out.println("Response test: "+response);
        Assert.assertNotEquals("[]", response);
    }




    @Test
    public final void testCreateInterface() {
        VirtualInterface v2 = new VirtualInterface();
        v2.setId("1");
        Entity<VirtualInterface> userEntity = Entity.entity(v2, MediaType.APPLICATION_JSON_TYPE);
        final Response response =       target("/interoperability/interfaces").request().post(userEntity);
        Assert.assertEquals(201, response.getStatus());
    }
    @Test
    public final void testUpdateInterface() {
        VirtualInterface v2 = new VirtualInterface();
        v2.setId("1");
        v2.setBase_url("created");
        Entity<VirtualInterface> userEntity = Entity.entity(v2, MediaType.APPLICATION_JSON_TYPE);
        Response response = target("/interoperability/interfaces").request().post(userEntity);
        v2.setBase_url("updated");
        userEntity = Entity.entity(v2, MediaType.APPLICATION_JSON_TYPE);
        response = target("/interoperability/interfaces").request().put(userEntity);
        Assert.assertEquals(201,response.getStatus());

    }
    @Test
    public final void testDelete() {
        VirtualInterface vd = new VirtualInterface();
        vd.setId("deleting");
        controller.addInterface(vd);
        Response response = target("/interoperability/interfaces/deleting").request().delete(Response.class);
        Assert.assertEquals(204, response.getStatus());  
    }
    @Test
    public final void testGet(){
        VirtualInterface vd = new VirtualInterface();
        vd.setId("testdelget");
        controller.addInterface(vd);
        Response response = target("/interoperability/interfaces/testdelget").request().get(Response.class);
        Assert.assertEquals(200,response.getStatus());


    }
    @Test
    public final void testEvents (){
        String response = target("/event/events").request().get(String.class);
        System.out.println("Response test: "+response);
        Assert.assertEquals("[]", response);

    }

    @Test
    public final void testEventId (){
        String    response = target("/event/1").request().get(String.class);
        System.out.println("Risposta all'id 1:"+response);
        Assert.assertEquals("null",response);
    }

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(9998);

    @Test
    public final void testGetSlas (){
        stubFor(get(urlEqualTo("/param/slas"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("Response content to get sta!")));

        VirtualInterface v2 = new VirtualInterface();
        v2.setId("param");
        v2.setDestination_port("9998");
        v2.setDestination_address("127.0.0.1");
        v2.setDestination_uri("127.0.0.1");
        v2.setBase_url("127.0.0.1");

        Entity<VirtualInterface> userEntity = Entity.entity(v2, MediaType.APPLICATION_JSON_TYPE);
        final Response response = target("/interoperability/interfaces").request().post(userEntity);
        Assert.assertEquals(201, response.getStatus());
        Response response1 = target("/param/slas").request().get(Response.class);       
        System.out.println("response test getSLAs: "+response1.getStatus());
        Assert.assertEquals(200, response1.getStatus());

    }
 
       @Test
       public final void testPostSlas(){
           stubFor(get(urlEqualTo("/param/slas"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to post sta!")));
           
           VirtualInterface v2 = new VirtualInterface();
           v2.setId("param");
           v2.setDestination_port("9998");
           v2.setDestination_address("127.0.0.1");
           v2.setDestination_uri("127.0.0.1");
           v2.setBase_url("127.0.0.1");
           Entity<VirtualInterface> userEntity = Entity.entity(v2, MediaType.APPLICATION_JSON_TYPE);
           final Response response = target("/interoperability/interfaces").request().post(userEntity);
           Assert.assertEquals(201, response.getStatus());
           Response response1 = target("/param/slas").request().post(userEntity);
           System.out.println("response test postSLAs: "+response.getStatus());
           Assert.assertEquals(201, response.getStatus());
       }
       
       

       @Test
       public final void testResource(){
           stubFor(get(urlEqualTo("/param/slas/0"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get sta!")));
          
           Response response1 = target("/param/slas/0").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testPostResource() {
           stubFor(get(urlEqualTo("/param/slas/0"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to put sta!")));
           Entity<String> userEntity = Entity.entity("this is a resource", MediaType.APPLICATION_JSON_TYPE);
           final Response response = target("/param/slas/0").request().put(
                  userEntity);
       Assert.assertEquals(200, response.getStatus());
       }
       @Test
       public final void testAlertResource(){
           stubFor(get(urlEqualTo("/param/slas/0/alerts"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get alerts!")));
           VirtualInterface v2 = new VirtualInterface();
           v2.setId("param");
           v2.setDestination_port("9998");
           v2.setDestination_address("127.0.0.1");
           v2.setDestination_uri("127.0.0.1");
           v2.setBase_url("127.0.0.1");
           Entity<VirtualInterface> userEntity = Entity.entity(v2, MediaType.APPLICATION_JSON_TYPE);
           final Response response = target("/interoperability/interfaces").request().post(userEntity);
           Response response1 = target("/param/slas/0/alerts").request().get(Response.class);
           System.out.println("risposta all'allert:"+response1.getEntity());
           Assert.assertEquals(200, response1.getStatus());
          
       }
       @Test
       public final void testAlertResourceParams(){
           stubFor(get(urlEqualTo("/param/slas/0/alerts/0"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get alerts!")));
           
           Response response = target("/param/slas/0/alerts/0").request().get(Response.class);
           Assert.assertEquals(200, response.getStatus());
       }
       @Test
       public final void testViolationResource(){
           stubFor(get(urlEqualTo("/param/slas/0/violations"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get violations!")));
           VirtualInterface v2 = new VirtualInterface();
           v2.setId("param");
           v2.setDestination_port("9998");
           v2.setDestination_address("127.0.0.1");
           v2.setDestination_uri("127.0.0.1");
           v2.setBase_url("127.0.0.1");
           Entity<VirtualInterface> userEntity = Entity.entity(v2, MediaType.APPLICATION_JSON_TYPE);
           final Response response = target("/interoperability/interfaces").request().post(userEntity);
           Response response1 = target("/param/slas/0/violations").request().get(Response.class);
           System.out.println("risposta alla violation:"+response1.getStatus());
           Assert.assertEquals(201, response.getStatus());
           
       }
       @Test
       public final void testViolationResourceParams(){
           stubFor(get(urlEqualTo("/param/slas/0/violations/0"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get violations!")));
         
           Response response1 = target("/param/slas/0/violations/0").request().get(Response.class);
           Assert.assertEquals(200, response1.getStatus());
       }
       @Test
       public final void testTemplatesResource(){
           stubFor(get(urlEqualTo("/param/slas/0/templates"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get templates!")));
           Response response1 = target("/param/slas/0/templates").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testTemplatesResourceParams(){
           stubFor(get(urlEqualTo("/param/slas/0/templates/0"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get templates!")));
           Response response1 = target("/param/slas/0/templates/0").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testAssociationResource(){
           stubFor(get(urlEqualTo("/param/slas/0/associations"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get associations!")));
           Response response1 = target("/param/slas/0/associations").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testAssociationResourceParams(){
           stubFor(get(urlEqualTo("/param/slas/0/associations/0"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get associations!")));
           Response response1 = target("/param/slas/0/associations/0").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testAlert(){
           
           stubFor(get(urlEqualTo("/param/slas/0/alert"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get an alert!")));
           Response response1 = target("/param/slas/0/alert").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testViolation(){         
           stubFor(get(urlEqualTo("/param/slas/0/violation"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get a violation!")));
           Response response1 = target("/param/slas/0/violation").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testTemplate(){          
           stubFor(get(urlEqualTo("/param/slas/0/template"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get a template!")));
           Response response1 = target("/param/slas/0/template").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testAssociation(){        
           stubFor(get(urlEqualTo("/param/slas/0/association"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get an association!")));
           Response response1 = target("/param/slas/0/association").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
       @Test
       public final void testState(){         
           stubFor(get(urlEqualTo("/param/slas/0/state"))
                   .willReturn(aResponse()
                           .withHeader("Content-Type", "text/plain")
                           .withBody("Response content to get a state!")));
           Response response1 = target("/param/slas/0/state").request().get(Response.class);
           Assert.assertEquals(200,response1.getStatus());
       }
//       @Test
//       public final void testPostAlerts(){
//         stubFor(get(urlEqualTo("/param/slas/0/alerts"))
//                   .willReturn(aResponse()
//                           .withHeader("Content-Type", "text/plain")
//                           .withBody("Response content to a POST on alerts!")));
//         IN_AlertsResource alert = new IN_AlertsResource(0);
//         Entity<IN_AlertsResource> userEntity = Entity.entity(alert, MediaType.APPLICATION_JSON_TYPE);       
//         final Response response = target("/param/slas/0/alerts").request().post(userEntity);
//                   Assert.assertEquals(201, response.getStatus());
//       }
//       @Test
//       public final void testPostViolations(){
//         stubFor(get(urlEqualTo("/param/slas/0/violations"))
//                   .willReturn(aResponse()
//                           .withHeader("Content-Type", "text/plain")
//                           .withBody("Response content to a POST on violations!")));
//         IN_ViolationsResource violation = new IN_ViolationsResource (0);
//         Entity<IN_ViolationsResource> userEntity = Entity.entity(violation, MediaType.APPLICATION_JSON_TYPE);   
//         final Response response = target("/param/slas/0/violations").request().post(userEntity);
//           Assert.assertEquals(201, response.getStatus());
//       }
//       @Test
//       public final void testPostTemplates(){
//         stubFor(get(urlEqualTo("/param/slas/0/templates"))
//                   .willReturn(aResponse()
//                           .withHeader("Content-Type", "text/plain")
//                           .withBody("Response content to a POST on templates!")));
//         IN_TemplatesResource template = new IN_TemplatesResource(0);
//         Entity<IN_TemplatesResource> userEntity = Entity.entity(template, MediaType.APPLICATION_JSON_TYPE);   
//         final Response response = target("/param/slas/0/templates").request().post(userEntity);
//           Assert.assertEquals(201, response.getStatus());
//       }
//       @Test
//       public final void testPostAssociations(){
//         IN_AssociationsResource association = new IN_AssociationsResource(0);
//         stubFor(get(urlEqualTo("/param/slas/0/associations"))
//                   .willReturn(aResponse()
//                           .withHeader("Content-Type", "text/plain")
//                           .withBody("Response content to a POST on templates!")));          
//         Entity<IN_AssociationsResource> userEntity = Entity.entity(association, MediaType.APPLICATION_JSON_TYPE);   
//         final Response response = target("/param/slas/0/associations").request().post(userEntity);
//           Assert.assertEquals(201, response.getStatus());
//       }
//     
}
